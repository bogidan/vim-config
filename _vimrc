set nocompatible
"source $VIMRUNTIME/vimrc_example.vim
"source $VIMRUNTIME/mswin.vim
"behave mswin

" Setup Plugins - https://github.com/junegunn/vim-plug
call plug#begin($VIM.'\plugged')
Plug 'rking/ag.vim'
Plug 'bling/vim-airline'
Plug 'godlygeek/tabular'
Plug 'will133/vim-dirdiff'
Plug 'vim-scripts/BufOnly.vim'
Plug 'Valloric/YouCompleteMe'
" Syntax
Plug 'othree/yajs.vim'
Plug 'zah/nim.vim'
Plug 'nachumk/systemverilog.vim'
Plug 'plasticboy/vim-markdown'
" Schemes
Plug 'fatih/molokai'
Plug 'altercation/vim-colors-solarized'
Plug 'zefei/cake16'
call plug#end()

" Airline Settings
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#show_tabs = 0
" Vim-Go
let g:go_highlight_space_tab_error = 0
let g:go_highlight_trailing_whitespace_error = 0
let g:go_fmt_autosave = 0
" Vim Markdown
let g:vim_markdown_folding_disabled=1

set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

" Editor Settings
set backspace=2
map <F1> <nop>
imap <F1> <Esc>
nnoremap <silent> <CR> :noh<CR><CR>
"nnoremap <silent> <CR> :let @/ = ""<CR><CR>

" Buffer Settings
set hidden
set autoread
nnoremap <F7> :bprev<CR>
nnoremap <F8> :bnext<CR>
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>

" Syntax Settings
syntax on
colorscheme desert

"" Common Vim
set hlsearch
" Easy open vimrc
command! Config :e $VIM/_vimrc
map <silent> <leader>conf :Config<CR>

set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab smartindent
set number
" stop ex mode
nnoremap Q <nop>

"" GVim Specialized
" Dbl-click highlight
nnoremap <silent> <2-LeftMouse> :let @/='\V\<'.escape(expand('<cword>'), '\').'\>'<cr>:set hls<cr>
"map <2-LeftMouse> *
"imap <2-LeftMouse> <Ctrl-o>*

" Stop files from being created
set backupdir=C:\WINDOWS\Temp\vimbackup,.
set directory=C:\WINDOWS\Temp\vimtemp,.

" Font Settings
set guifont=Consolas

" To enable the saving and restoring of screen positions.
let g:screen_size_restore_pos = 1
set guioptions-=T   " remove toolbar
set guioptions-=r   " right scrollbar
set guioptions-=L   " left scroll bar
set guioptions+=m   " show menubar
set mouse=a
set lines=60 columns=200

" Completion
filetype plugin on
set omnifunc=syntaxcomplete#Complete
inoremap <C-Space> <C-x><C-o>

